from django.shortcuts import render
from .models import Image
from django.conf import settings

# Create your views here.
def index(request):
	response = {}
	return render(request, 'landingpage.html')

def post_image(request):
	if request.method == 'POST':
		files = request.FILES['photos']
		image = Image(img=files)
		image.save()
	image_dirs = '/media/'+ str(image.img)
	print(image_dirs)
	response = {'img':image_dirs}
	return render(request, 'result.html', response)
	
	